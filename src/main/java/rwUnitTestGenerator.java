import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Set;

public class rwUnitTestGenerator {

    public static void main(String[] args) {
        try {
            //create("Input.json", "Null.txt");
            create("Input2.json","Null2.txt");
        } catch (Exception e) {
                System.out.println("Error");
        }
    }

    private static void create(String inputLocation, String outputLocation) throws Exception {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(inputLocation)); //JSON READER
        FileWriter extFileWriter = new FileWriter(outputLocation);
        BufferedWriter fileWriter = new BufferedWriter(extFileWriter);

        JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);
        Set<String> jsonkeys = jsonObject.keySet();

        for (String key : jsonkeys) {
            if (jsonObject.get(key).isJsonObject()) {
                JsonObject object = jsonObject.get(key).getAsJsonObject();
                fileWriter.append(" * ").append(key);
                handleJsonObjects(object, fileWriter);
            } else if (jsonObject.get(key).isJsonArray()) {
                fileWriter.append("* ").append(key);
                JsonArray object = jsonObject.get(key).getAsJsonArray();
                handleJsonArray(object, fileWriter);
            } else if (jsonObject.get(key).isJsonNull()) {
                fileWriter.append("* ").append(key);
                JsonNull jsonNull = jsonObject.get(key).getAsJsonNull();
            } else if (jsonObject.get(key).isJsonPrimitive()) {
                fileWriter.append("* ").append(key).append(" ").append(jsonObject.get(key).getAsString()); // PRIMITIVES HAVE VALUES NO OTHER ROUTE HAS A VALUE, ALL HAVE .SIZE()
            }
            fileWriter.newLine();
        }
        fileWriter.close();
    }

    private static void handleJsonObjects(JsonObject jsonObject, BufferedWriter fileWriter) throws Exception {
        Set<String> jsonKeys = jsonObject.keySet();
        for (String key : jsonKeys) {
            if (jsonObject.get(key).isJsonObject()) {
                JsonObject object = jsonObject.get(key).getAsJsonObject();
                fileWriter.append("* ").append(key);
                handleJsonObjects(object, fileWriter);
            } else if (jsonObject.get(key).isJsonArray()) {
                fileWriter.append("* ").append(key);
                JsonArray object = jsonObject.get(key).getAsJsonArray();
                handleJsonArray(object, fileWriter);
            } else if (jsonObject.get(key).isJsonNull()) {
                fileWriter.append("* ").append(key);
                JsonNull jsonNull = jsonObject.get(key).getAsJsonNull();
            } else if (jsonObject.get(key).isJsonPrimitive()) {
                fileWriter.append("* ").append(key).append(" ").append(jsonObject.get(key).getAsString()); // PRIMITIVES HAVE VALUES NO OTHER ROUTE HAS A VALUE, ALL HAVE .SIZE()
            }
        }
        fileWriter.newLine();

    }

    private static void handleJsonArray(JsonArray jsonArray, BufferedWriter fileWriter) throws Exception {
        System.out.println(jsonArray.size());
        for (int i = 0; i >= jsonArray.size(); i++) {
            if (jsonArray.get(i).isJsonObject()) {
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();

                Set<String> jsonKeys = jsonObject.keySet();
                for (String key : jsonKeys) {
                    if (jsonObject.get(key).isJsonObject()) {
                        JsonObject object = jsonObject.get(key).getAsJsonObject();
                        fileWriter.append("* ").append(key);
                        handleJsonObjects(object, fileWriter);
                    } else if (jsonObject.get(key).isJsonArray()) {
                        fileWriter.append("* ").append(key);
                        JsonArray object = jsonObject.get(key).getAsJsonArray();
                        handleJsonArray(object, fileWriter);
                    } else if (jsonObject.get(key).isJsonNull()) {
                        fileWriter.append("* ").append(key);
                        JsonNull jsonNull = jsonObject.get(key).getAsJsonNull();
                    } else if (jsonObject.get(key).isJsonPrimitive()) {
                        fileWriter.append("* ").append(key).append(" ").append(jsonObject.get(key).getAsString()); // PRIMITIVES HAVE VALUES NO OTHER ROUTE HAS A VALUE, ALL HAVE .SIZE()
                    }
                }
                fileWriter.newLine();
            } else if (jsonArray.get(i).isJsonPrimitive()) {
                fileWriter.append("* ").append(jsonArray.get(i).getAsString());
            }
        }
    }
}